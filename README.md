### Frontend Dev Starter Kit


### Getting started
Clone project
````bash
git clone https://${user}@bitbucket.org/Adrianoleal/tw-frontend-dev-starter-kit.git
````

Install dependencies
````bash
npm install

sudo gem install susy
````

Start development mode [http://localhost:8888](http://localhost:8888)
````bash
npm start
````

Build only
````bash
npm build
````


### Important
- Don't modify the file names and the folder names
    - js/index.js
    - styles/index.css
- Don't modify these lines on index.html, the build process create an index.js and index.css bundle files
````html
<link rel="stylesheet" href="./index.css" />
<script src="./index.js"></script>
````
### Useful links
- [Susy - Sass](http://susydocs.oddbird.net/en/latest/toolkit/)
- [Flexsliders](http://flexslider.woothemes.com/)
- [BEM convention](http://getbem.com/naming/)
- [BEM convention](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)
