/**
 * Created by alga on 23/1/17.
 */

const gulp = require('gulp');
const addsrc = require('gulp-add-src');
const babel = require('gulp-babel');
const del = require('del');
const sass = require('gulp-sass');
const browserify  = require('browserify');
const babelify    = require('babelify');
const source      = require('vinyl-source-stream');
const concat = require('gulp-concat');
const insert = require('gulp-insert');

// global vars
const port = 8888;

// cleanup the dist folder
gulp.task('default',function () {
    del(['./dist/**/*']).then(()=>{
        gulp.start('tasks');
    })
});

gulp.task('tasks',[
    'build-tasks',
    'watch'
]);

gulp.task('build',function () {
    del(['./dist/**/*']).then(()=>{
        gulp.start('build-tasks');
    })
});

gulp.task('build-tasks',[
    'js',
    'sass',
    'html'
]);

gulp.task('js', function(cb) {
    // compile for browser
    return browserify({entries: './src/js/index.js', debug: true})
        .transform("babelify", { presets: ["es2015"] })
        .bundle()
        .pipe(source('index.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('sass', function() {
    return gulp
        .src('./src/styles/**/*.{scss,sass}')
        .pipe(concat('index.scss'))
        .pipe(sass({
            // outputStyle: 'compressed',
            includePaths: ['node_modules/susy/sass','./src/styles']
        }).on('error', sass.logError))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('html', function(cb) {
    return gulp.src(['./src/**/*.html'])
        .pipe(gulp.dest('./dist/'));
});

gulp.task('dev-socket-copy', function(cb) {
    return gulp.src(['./node_modules/socket.io-client/dist/socket.io.js'])
        .pipe(gulp.dest('./dist/'));
});

gulp.task('dev-socket-html', function() {
    return gulp.src('./src/index.html')
        .pipe(insert.append(socketClient))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function() {
    // -- real time socket injection ------------------
    gulp.run(['dev-socket-copy','dev-socket-html']);

    const express = require('express');
    const app = express();
    const server = require('http').Server(app);
    const io = require('socket.io')(server);

    server.listen(port, function () {
        console.log(`Running on http://localhost:${port}`)
    });

    app.use('/', express.static('./dist'));

    let clientSocket = null;
    io.on('connection', function (socket) {
        clientSocket = socket;
        socket.emit('news', { hello: 'world' });
        socket.on('my other event', function (data) {
            console.log(data);
        });
    });
    // -- real time socket injection ------------------

    // Watches the scss folder for all .scss and .sass files
    // If any file changes, run the sass task
    let sass = gulp.watch('./src/styles/**/*.{scss,sass}', ['sass']);
    let js = gulp.watch('./src/**/*.{js,jsx}', ['js']);
    let html = gulp.watch('./src/**/*.html', ['html']);

    sass.on('change', function(event) {
        // console.log(event);
        console.log('File ' + event.path + ' was ' + event.type);
        clientSocket.emit('news', { change: 'File ' + event.path + ' was ' + event.type });
    });

    js.on('change', function(event) {
        // console.log(event);
        console.log('File ' + event.path + ' was ' + event.type);
        clientSocket.emit('news', { change: 'File ' + event.path + ' was ' + event.type });
    });

    html.on('change', function(event) {
        // console.log(event);
        console.log('File ' + event.path + ' was ' + event.type);
        clientSocket.emit('news', { change: 'File ' + event.path + ' was ' + event.type });
    });
});

// -- real time socket injection ------------------
socketClient = `
        <script src="./socket.io.js"></script>
        <script>
            var socket = io.connect('http://localhost:${port}');
            socket.on('news', function (data) {
                console.log(data);
                if(data.change){
                    var reload = function(){ window.location.reload(); };
                    setTimeout(reload,1500);
                }
            });
        </script>
    `;
// -- real time socket injection ------------------
