/**
 * Created by alga on 23/1/17.
 */


class HelloWorld {
    constructor(opt={}) {
        this._opt = opt;
    }

    get(){
        return "Hello World";
    }
}

export { HelloWorld }
