/**
 * Created by alga on 23/1/17.
 */

import libs from './libs'
import { HelloWorld } from './hello-world'


const helloWorld = new HelloWorld();

class Index {
    constructor(opt){
        this._opt = opt;

        console.log("Index class demo constructor");

        this.helloWorld();
    }

    helloWorld(){
        console.log("helloWorld", $('.content'));
        $('.content').text(helloWorld.get());
    }
}

new Index();
